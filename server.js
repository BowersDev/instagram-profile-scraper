const express = require('express');
const request = require('request');
const cheerio = require('cheerio');
const path = require("path");


const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/public/index.html'));
});


app.get('/api/get-user', (req,res)=>{
	var username = req.query.username;

	getUserData(username).then((userData)=>{
		res.send(userData);

	}).catch((err)=>{
		res.send(JSON.stringify( {error:err} ));
	})
});




app.listen(port);
console.log('Listening on port ' + port);

app.use(express.static('./public'));






function getUserData(username){
	return new Promise((resolve,reject)=>{

		request(`https://www.instagram.com/${username}/`, (error,response,html) => {
			if (!error && response.statusCode == 200) {
		        const $ = cheerio.load(html);
		        var _scripts = $('script[type="text/javascript"]');
		        _scripts.each((index, script) => {
		            if (script.children.length >= 1) {
		                if (script.children[0].data != undefined) {
		                    if (script.children[0].data.toString().includes('window._sharedData = {')) {
		                        let data = script.children[0].data.split('window._sharedData = ');
		                        let jsonData = JSON.parse(data[1].replace(';', ''));
		                        let user = jsonData.entry_data.ProfilePage[0].graphql.user;

								return resolve(user);
		                    }
		                }
		            }

		        });
		    }
			reject('ERROR requesting user data');
		});

	});
}
