
document.getElementById('getProfile').addEventListener('click', (ev)=>{
	var username = document.getElementById('username').value;
	if (username){
		username = encodeURIComponent(username); // encode it to be url friendly

		var XHR = new XMLHttpRequest();
		XHR.onreadystatechange = function(){
			if (this.readyState==4 && this.status==200){

				var userData = JSON.parse(this.responseText);
				if (userData.err){
					console.log('ERROR getting user data');
				}else{
					// DO STUFF WITH THE USER DATA HEREEEEEEEEEEEEEEEEE
					console.log(userData);
					document.getElementById('output').innerHTML = JSON.stringify(userData);

					// example... get their profile pic and display it in an image
					document.getElementById('profilePic').src = userData.profile_pic_url_hd;
				}

			}
		}

		XHR.open("GET", `api/get-user?username=${username}`, true);
		XHR.send();

	}
});
